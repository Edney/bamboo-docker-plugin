package com.atlassian.bamboo.plugins.docker.upgrade;

import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.deployments.environments.service.EnvironmentService;
import com.atlassian.bamboo.deployments.environments.service.EnvironmentTaskService;
import com.atlassian.bamboo.plan.PlanPredicates;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutableJob;
import com.atlassian.bamboo.plugin.BambooPluginUtils;
import com.atlassian.bamboo.plugins.docker.PluginConstants;
import com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator;
import com.atlassian.bamboo.task.TaskConfigurationService;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Upgrade task to set default volume and work dir configuration in existing docker tasks
 */
public class ConfigurableVolumesAndWorkDirUpgradeTask implements PluginUpgradeTask
{
    private static final Logger log = LoggerFactory.getLogger(ConfigurableVolumesAndWorkDirUpgradeTask.class);

    private final CachedPlanManager planManager;
    private final TaskConfigurationService taskConfigurationService;
    private final EnvironmentService environmentService;
    private final EnvironmentTaskService environmentTaskService;

    public ConfigurableVolumesAndWorkDirUpgradeTask(@NotNull final CachedPlanManager planManager, @NotNull final TaskConfigurationService taskConfigurationService,
                                                    @NotNull final EnvironmentService environmentService, @NotNull final EnvironmentTaskService environmentTaskService)
    {
        this.planManager = planManager;
        this.taskConfigurationService = taskConfigurationService;
        this.environmentService = environmentService;
        this.environmentTaskService = environmentTaskService;
    }

    @Override
    public int getBuildNumber()
    {
        return 1;
    }

    @Override
    public String getShortDescription()
    {
        return "Set default volume and work dir configuration in existing docker tasks.";
    }

    @Override
    public Collection<Message> doUpgrade() throws Exception
    {
        log.info("Starting upgrade task to set default volume and work dir configuration in existing docker tasks.");

        upgradeJobs();
        upgradeEnvironments();

        log.info("Finished upgrade task to set default volume and work dir configuration in existing docker tasks.");

        return Collections.emptyList();
    }

    private void upgradeJobs()
    {
        final List<ImmutableJob> jobs = planManager.getPlans(ImmutableJob.class, Predicates.not(PlanPredicates.planHasMaster()));

        int numberOfTasksUpgradedInJobs = 0;

        for (ImmutableJob job : jobs)
        {
            for (TaskDefinition taskDefinition : BambooPluginUtils.filterTasks(job, DockerCliTaskConfigurator.DOCKER_CLI_TASK_KEY))
            {
                upgradeTaskDefinition(taskDefinition);
                taskConfigurationService.editTask(job.getPlanKey(), taskDefinition.getId(), taskDefinition.getUserDescription(), taskDefinition.isEnabled(),
                                                  taskDefinition.getConfiguration(), taskDefinition.getRootDirectorySelector());
                numberOfTasksUpgradedInJobs++;
            }
        }

        log.info(String.format("Upgraded %d docker tasks in %d jobs.", numberOfTasksUpgradedInJobs, jobs.size()));
    }

    private void upgradeEnvironments()
    {
        final Iterable<Environment> environments = environmentService.getAllEnvironmentsNoUserContext();

        int numberOfTasksUpgradedInEnvironments = 0;

        for (Environment environment : environments)
        {
            for (TaskDefinition taskDefinition : BambooPluginUtils.filterTasks(environment.getTaskDefinitions(), DockerCliTaskConfigurator.DOCKER_CLI_TASK_KEY))
            {
                upgradeTaskDefinition(taskDefinition);
                environmentTaskService.editTask(environment.getId(), taskDefinition.getId(), taskDefinition.getUserDescription(), taskDefinition.isEnabled(),
                                                taskDefinition.getConfiguration());
                numberOfTasksUpgradedInEnvironments++;
            }
        }

        log.info(String.format("Upgraded %d docker tasks in %d environments.", numberOfTasksUpgradedInEnvironments, Iterables.size(environments)));
    }

    private void upgradeTaskDefinition(TaskDefinition taskDefinition)
    {
        final Map<String, String> taskConfig = taskDefinition.getConfiguration();
        taskConfig.put(DockerCliTaskConfigurator.WORK_DIR, DockerCliTaskConfigurator.DEFAULT_CONTAINER_WORK_DIR);
        taskConfig.put(DockerCliTaskConfigurator.HOST_DIRECTORY_PREFIX + 0, DockerCliTaskConfigurator.TASK_WORK_DIR_PLACEHOLDER);
        taskConfig.put(DockerCliTaskConfigurator.CONTAINER_DATA_VOLUME_PREFIX + 0, DockerCliTaskConfigurator.DEFAULT_CONTAINER_WORK_DIR);
    }

    @Override
    public String getPluginKey()
    {
        return PluginConstants.DOCKER_PLUGIN_KEY;
    }
}
