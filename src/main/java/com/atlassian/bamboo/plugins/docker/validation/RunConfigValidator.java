package com.atlassian.bamboo.plugins.docker.validation;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator;
import com.atlassian.bamboo.task.TaskContextHelperService;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.util.NumberUtils;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import static com.atlassian.bamboo.plugins.docker.tasks.DockerTaskPredicates.isConfigurationFieldEqual;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.isDockerRunDetachedTask;
import static com.atlassian.bamboo.util.NumberUtils.isPositiveInteger;
import static com.atlassian.bamboo.utils.BambooPredicates.hasTaskDefinitionEqualId;
import static com.atlassian.bamboo.utils.predicates.TextPredicates.startsWith;

public class RunConfigValidator implements ConfigValidator
{
    private static final String TASK_ID = "taskId";

    private static String NAME_REGEXP = "[a-zA-Z0-9][a-zA-Z0-9_.-]*";
    private static final Pattern NAME_PATTERN = Pattern.compile(NAME_REGEXP);

    private static final int MAX_PORT = 65535;

    private final I18nResolver i18nResolver;
    private final TaskContextHelperService taskContextHelper;

    public RunConfigValidator(@NotNull final I18nResolver i18nResolver, @NotNull final TaskContextHelperService taskContextHelper)
    {
        this.i18nResolver = i18nResolver;
        this.taskContextHelper = taskContextHelper;
    }

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection)
    {
        if (StringUtils.isBlank(params.getString(DockerCliTaskConfigurator.IMAGE)))
        {
            errorCollection.addError(DockerCliTaskConfigurator.IMAGE, i18nResolver.getText("docker.image.error.empty"));
        }

        if (params.getBoolean(DockerCliTaskConfigurator.DETACH))
        {
            if (StringUtils.isBlank(params.getString(DockerCliTaskConfigurator.NAME)))
            {
                errorCollection.addError(DockerCliTaskConfigurator.NAME, i18nResolver.getText("docker.name.error.empty"));
            }
            else if (!NAME_PATTERN.matcher(params.getString(DockerCliTaskConfigurator.NAME)).matches())
            {
                errorCollection.addError(DockerCliTaskConfigurator.NAME, i18nResolver.getText("docker.name.error.invalid"));
            }
            else if (!isNameUniqueInJob(params))
            {
                errorCollection.addError(DockerCliTaskConfigurator.NAME, i18nResolver.getText("docker.name.error.duplicate"));
            }

            final Map<String, String> hostPorts = getPorts(params, DockerCliTaskConfigurator.HOST_PORT_PREFIX);
            final Map<String, String> containerPorts = getPorts(params, DockerCliTaskConfigurator.CONTAINER_PORT_PREFIX);

            validatePorts(hostPorts, true, errorCollection);
            validatePorts(containerPorts, false, errorCollection);

            if (params.getBoolean(DockerCliTaskConfigurator.SERVICE_WAIT))
            {
                if (hostPorts.size() == 0)
                {
                    errorCollection.addError(DockerCliTaskConfigurator.SERVICE_WAIT, i18nResolver.getText("docker.service.wait.error.ports"));
                }

                if (StringUtils.isBlank(params.getString(DockerCliTaskConfigurator.SERVICE_URL_PATTERN)))
                {
                    errorCollection.addError(DockerCliTaskConfigurator.SERVICE_URL_PATTERN, i18nResolver.getText("docker.service.url.pattern.error.empty"));
                }

                if (!NumberUtils.isPositiveInteger(params.getString(DockerCliTaskConfigurator.SERVICE_TIMEOUT)))
                {
                    errorCollection.addError(DockerCliTaskConfigurator.SERVICE_TIMEOUT, i18nResolver.getText("docker.service.timeout.error.invalid"));
                }
            }
        }

        validateVolumes(params, errorCollection);
    }

    private boolean isNameUniqueInJob(@NotNull final ActionParametersMap params)
    {
        final String name = params.getString(DockerCliTaskConfigurator.NAME);
        final long taskId = params.getLong(TASK_ID, -1);

        final Predicate<TaskDefinition> isDockerRunDetachedNameEqual = Predicates.and(
                isDockerRunDetachedTask,
                isConfigurationFieldEqual(DockerCliTaskConfigurator.NAME, name),
                Predicates.not(hasTaskDefinitionEqualId(taskId)));

        return Iterables.isEmpty(Iterables.filter(taskContextHelper.getTasks(params), isDockerRunDetachedNameEqual));
    }

    private Map<String, String> getPorts(@NotNull final ActionParametersMap params, @NotNull final String portPrefix)
    {
        final ImmutableMap.Builder<String, String> ports = ImmutableMap.builder();

        for (String portKey : Iterables.filter(params.keySet(), startsWith(portPrefix)))
        {
            ports.put(portKey, params.getString(portKey));
        }

        return ports.build();
    }

    private void validatePorts(@NotNull final Map<String, String> ports, final boolean optional, @NotNull final ErrorCollection errorCollection)
    {
        final Set<String> definedPorts = Sets.newHashSet();

        for (Map.Entry<String, String> port : ports.entrySet())
        {
            if (StringUtils.isBlank(port.getValue()))
            {
                if (!optional)
                {
                    errorCollection.addError(port.getKey(), i18nResolver.getText("docker.ports.container.error.empty"));
                }
            }
            else if (!isPositiveInteger(port.getValue()) || Integer.parseInt(port.getValue()) > MAX_PORT)
            {
                errorCollection.addError(port.getKey(), i18nResolver.getText("docker.ports.error.invalid"));
            }
            else if (definedPorts.contains(port.getValue()))
            {
                errorCollection.addError(port.getKey(), i18nResolver.getText("docker.ports.error.duplicate"));
            }

            definedPorts.add(port.getValue());
        }
    }

    private Set<String> validateVolumes(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection)
    {
        final Set<String> definedDataVolumes = Sets.newHashSet();

        for (String key : Iterables.filter(params.keySet(), startsWith(DockerCliTaskConfigurator.CONTAINER_DATA_VOLUME_PREFIX)))
        {
            final String containerDataVolume = params.getString(key);

            if (StringUtils.isBlank(containerDataVolume))
            {
                errorCollection.addError(key, i18nResolver.getText("docker.volumes.containerDataVolume.error.empty"));
            }
            else if (definedDataVolumes.contains(containerDataVolume))
            {
                errorCollection.addError(key, i18nResolver.getText("docker.volumes.containerDataVolume.error.duplicate"));
            }

            definedDataVolumes.add(containerDataVolume);
        }

        return definedDataVolumes;
    }
}
