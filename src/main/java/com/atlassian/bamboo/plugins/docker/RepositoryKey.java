package com.atlassian.bamboo.plugins.docker;

import com.atlassian.bamboo.utils.Pair;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.regex.Pattern;

public class RepositoryKey
{
    private static final Logger log = Logger.getLogger(RepositoryKey.class);

    public static final String DEFAULT_TAG = "latest";

    // only [a-z0-9_] are allowed, size between 4 and 30
    private static final String NAMESPACE_REGEXP = "[a-z0-9_]{4,30}";
    public static final Pattern NAMESPACE_PATTERN = Pattern.compile(NAMESPACE_REGEXP);

    // only [a-zA-Z0-9_.-] are allowed
    private static final String REPOSITORY_REGEXP = "[a-zA-Z0-9_.-]*";
    public static final Pattern REPOSITORY_PATTERN = Pattern.compile(REPOSITORY_REGEXP);

    private final String registry;
    private final String namespace;
    private final String repository;
    private final String tag;

    private RepositoryKey(@NotNull final RepositoryKeyBuilder builder)
    {
        this.registry = builder.registry;
        this.namespace = builder.namespace;
        this.repository = builder.repository;
        this.tag = builder.tag;
    }

    @Nullable
    public String getRegistry()
    {
        return registry;
    }

    @Nullable
    public String getNamespace()
    {
        return namespace;
    }

    @NotNull
    public String getRepository()
    {
        return repository;
    }

    @NotNull
    public String getTag()
    {
        return tag;
    }

    @NotNull
    public String getEncodedRepositoryString()
    {
        return new StringBuilder()
                .append(getRepositoryPart())
                .append(':').append(getEncodedTagPart())
                .toString();
    }

    @NotNull
    public Pair<String, String> getRepositoryTagParts()
    {
        return Pair.make(getRepositoryPart(), getEncodedTagPart());
    }

    private String getRepositoryPart()
    {
        final StringBuilder repositoryBuilder = new StringBuilder();

        if (registry != null)
        {
            repositoryBuilder.append(registry).append('/');
        }

        if (namespace != null)
        {
            repositoryBuilder.append(namespace).append('/');
        }

        repositoryBuilder.append(repository);

        return repositoryBuilder.toString();
    }

    @Nullable
    private String getEncodedTagPart()
    {
        try
        {
            return URIUtil.encodeQuery(tag);
        }
        catch (URIException e)
        {
            log.error(String.format("Encoding of tag (%s) failed.", tag), e);
            return tag;
        }
    }

    static class RepositoryKeyBuilder
    {
        private String registry;
        private String namespace;
        private final String repository;
        private String tag = DEFAULT_TAG;

        public RepositoryKeyBuilder(@NotNull final String repository)
        {
            this.repository = repository;
        }

        @NotNull
        public RepositoryKeyBuilder registry(@Nullable final String registry)
        {
            this.registry = registry;
            return this;
        }

        @NotNull
        public RepositoryKeyBuilder namespace(@Nullable final String namespace)
        {
            this.namespace = namespace;
            return this;
        }

        @NotNull
        public RepositoryKeyBuilder tag(@NotNull final String tag)
        {
            this.tag = tag;
            return this;
        }

        @NotNull
        public RepositoryKey build()
        {
            return new RepositoryKey(this);
        }
    }
}