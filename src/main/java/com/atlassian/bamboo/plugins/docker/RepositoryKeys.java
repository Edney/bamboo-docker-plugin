package com.atlassian.bamboo.plugins.docker;

import com.atlassian.fugue.Option;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import static com.atlassian.bamboo.plugins.docker.RepositoryKey.DEFAULT_TAG;
import static com.atlassian.bamboo.plugins.docker.RepositoryKey.RepositoryKeyBuilder;

public abstract class RepositoryKeys
{
    private static final String REGISTRY_MARKERS = ".:";

    /**
     * Parses the given String in to a {@link RepositoryKey}.
     * <p>
     * This method splits the string in to 4 parts as represented by the following form: "registry/namespace/repository:tag".
     * It does not perform any validation so the caller should validate the resulting {@link RepositoryKey} before use.
     *
     * @param repositoryKey The string to parse.
     */
    @NotNull
    public static RepositoryKey parseKey(@NotNull final String repositoryKey)
    {
        return new RepositoryKeyBuilder(parseRepository(repositoryKey))
                .registry(parseRegistry(repositoryKey).getOrNull())
                .namespace(parseNamespace(repositoryKey).getOrNull())
                .tag(parseTag(repositoryKey).getOrElse(DEFAULT_TAG))
                .build();
    }

    @NotNull
    private static Option<String> parseRegistry(@NotNull final String repositoryKey)
    {
        if (includesRegistry(repositoryKey))
        {
            return Option.option(StringUtils.substringBefore(repositoryKey, "/"));
        }
        return Option.none();
    }

    @NotNull
    private static Option<String> parseNamespace(@NotNull final String repositoryKey)
    {
        final String namespaceRepositoryTag = stripRegistry(repositoryKey);
        if (StringUtils.contains(namespaceRepositoryTag, '/'))
        {
            return Option.option(StringUtils.substringBefore(namespaceRepositoryTag, "/"));
        }
        return Option.none();
    }

    @NotNull
    private static String parseRepository(@NotNull final String repositoryKey)
    {
        final String namespaceRepositoryTag = stripRegistry(repositoryKey);
        if (StringUtils.contains(namespaceRepositoryTag, '/'))
        {
            final String repositoryAndTag = StringUtils.substringAfter(namespaceRepositoryTag, "/");
            return StringUtils.substringBeforeLast(repositoryAndTag, ":");
        }
        return StringUtils.substringBeforeLast(namespaceRepositoryTag, ":");
    }

    @NotNull
    private static Option<String> parseTag(@NotNull final String repositoryKey)
    {
        final String namespaceRepositoryTag = stripRegistry(repositoryKey);
        if (StringUtils.contains(namespaceRepositoryTag, ':'))
        {
            return Option.option(StringUtils.substringAfterLast(namespaceRepositoryTag, ":"));
        }
        return Option.none();
    }

    @NotNull
    private static String stripRegistry(@NotNull final String repositoryKey)
    {
        if (includesRegistry(repositoryKey))
        {
            return StringUtils.substringAfter(repositoryKey, "/");
        }
        return repositoryKey;
    }

    private static boolean includesRegistry(@NotNull final String repositoryKey)
    {
        if (StringUtils.contains(repositoryKey, '/'))
        {
            final String registryOrNamespace = StringUtils.substringBefore(repositoryKey, "/");
            return StringUtils.containsAny(registryOrNamespace, REGISTRY_MARKERS);
        }
        return false;
    }
}