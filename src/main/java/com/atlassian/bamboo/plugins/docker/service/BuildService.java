package com.atlassian.bamboo.plugins.docker.service;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.plugins.docker.RepositoryKey;
import com.atlassian.bamboo.plugins.docker.RepositoryKeys;
import com.atlassian.bamboo.plugins.docker.client.BuildConfig;
import com.atlassian.bamboo.plugins.docker.client.Docker;
import com.atlassian.bamboo.plugins.docker.config.BuildConfiguration;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;

public class BuildService implements DockerService
{
    private final Docker docker;

    public BuildService(Docker docker)
    {
        this.docker = docker;
    }

    @Override
    public void execute(@NotNull final CommonTaskContext taskContext) throws TaskException
    {
        final BuildLogger logger = taskContext.getBuildLogger();
        final BuildConfiguration buildConfig = BuildConfiguration.fromContext(taskContext);

        if (buildConfig.isDockerfileInline())
        {
            writeDockerfile(buildConfig);
        }

        try
        {
            docker.build(taskContext.getWorkingDirectory(), buildConfig.getRepository(), BuildConfig.builder().nocache(buildConfig.isNocache()).build());
            logger.addBuildLogEntry(String.format("Built Docker image '%s'", buildConfig.getRepository()));

            if (buildConfig.isSave())
            {
                final RepositoryKey repositoryKey = RepositoryKeys.parseKey(buildConfig.getRepository());
                docker.save(buildConfig.getFilename(), repositoryKey.getEncodedRepositoryString());
                logger.addBuildLogEntry(String.format("Saved Docker image '%s' to file '%s'", buildConfig.getRepository(),
                                                      new File(taskContext.getWorkingDirectory(), buildConfig.getFilename()).getAbsolutePath()));
            }
        }
        catch (Exception e)
        {
            throw new TaskException("Failed to execute task", e);
        }
    }

    private void writeDockerfile(@NotNull final BuildConfiguration config) throws TaskException
    {
        try
        {
            File dockerFile = new File(config.getWorkingDirectory(), "Dockerfile");
            Files.write(config.getDockerfile(), dockerFile, Charsets.UTF_8);
        }
        catch (IOException e)
        {
            throw new TaskException("Unable to create Dockerfile", e);
        }
    }
}