package com.atlassian.bamboo.plugins.docker;

import com.atlassian.bamboo.v2.build.agent.capability.AbstractExecutableCapabilityTypeModule;
import com.google.common.collect.Lists;

import java.util.List;

public class DockerCapabilityTypeModule extends AbstractExecutableCapabilityTypeModule
{
    public static final String DOCKER_CAPABILITY = "system.docker.executable";
    public static final String DOCKER_EXECUTABLE = "dockerExecutable";

    private static final String CAPABILITY_TYPE_ERROR_UNDEFINED_EXECUTABLE = "docker.error.undefinedExecutable";

    @Override
    public String getExecutableKey()
    {
        return DOCKER_EXECUTABLE;
    }

    @Override
    public String getCapabilityUndefinedKey()
    {
        return CAPABILITY_TYPE_ERROR_UNDEFINED_EXECUTABLE;
    }

    @Override
    public String getMandatoryCapabilityKey()
    {
        return DOCKER_CAPABILITY;
    }

    @Override
    public List<String> getDefaultWindowPaths()
    {
        return Lists.newArrayList();
    }

    @Override
    public String getExecutableFilename()
    {
        return "docker";
    }

    public String getExecutableDescription()
    {
        return getText(AGENT_CAPABILITY_TYPE_PREFIX + DOCKER_CAPABILITY + ".description");
    }
}