[@ww.radio labelKey='docker.registry' name='registryOption' toggle='true'
list='registryOptions' listKey='first' listValue='second' /]
[@s.textfield labelKey='docker.push.repository' name='pushRepository' cssClass="long-field" required=true /]
[@ui.bambooSection dependsOn='registryOption' showOn='hub']
    <div class="description">[@s.text name='docker.push.repository.hub.description' /]</div>
[/@ui.bambooSection]
[@ui.bambooSection dependsOn='registryOption' showOn='custom']
    <div class="description">[@s.text name='docker.push.repository.custom.description' /]</div>
[/@ui.bambooSection]
<br/>
[@ui.bambooSection titleKey="docker.credentials"]
    <div class="description">[@s.text name='docker.credentials.description'/]</div>
    [@s.textfield labelKey='docker.username' name='username' cssClass="long-field" /]
    [#if password?has_content]
        [@s.checkbox labelKey='docker.password.change' toggle=true name='changePassword' /]
        [@ui.bambooSection dependsOn='changePassword']
            [@s.password labelKey='docker.password' name='password' cssClass="long-field" /]
        [/@ui.bambooSection]
    [#else]
        [@s.hidden name='changePassword' value='true' /]
        [@s.password labelKey='docker.password' name='password' cssClass="long-field" /]
    [/#if]
    [@s.textfield labelKey='docker.email' name='email' cssClass="long-field" /]
[/@ui.bambooSection]